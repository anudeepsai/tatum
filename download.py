import io
import requests
import boto3
from botocore.exceptions import ClientError
from flask import Flask
import os

from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)


@app.route("/")
def index():
    get_data()
    return "data processing done"


if __name__ == "__main__":
    print("hi terminal")
    app.run(host="0.0.0.0", port=80)
else:
    print("bye")

api_key = os.environ.get(API_KEY)
aws_access_key_id = os.environ.get(AWS_ACCESS_KEY_ID)
aws_secret_access_key = os.environ.get(AWS_SECRET_ACCESS_KEY)
s3_bucket_name = os.environ.get(S3_BUCKET_NAME)
complete_url = os.environ.get(COMPLETE_URL)
file_name = os.environ.get(FILE_NAME)

# complete_url = 'http://bulk.openweathermap.org/sample/hourly_14.json.gz'
def get_data():
    response = requests.get(complete_url, stream=True)
    if response.status_code == 200:
        print(response)

    s3_client = boto3.client(
        "s3",
        region_name="us-east-1",
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
    )

    try:
        s3_client.upload_fileobj(
            Fileobj=io.BytesIO(response.raw.read()),
            Bucket=s3_bucket_name,
            Key=file_name,
        )
    except ClientError as e:
        print(e)

    resp = s3_client.select_object_content(
        Bucket=s3_bucket_name,
        Key=file_name,
        Expression="SELECT s.* FROM S3Object s WHERE s.city.name = 'Kathmandu'",  # You will need to fiddle with the quotes on the SQL here.
        ExpressionType="SQL",
        InputSerialization={"CompressionType": "GZIP", "JSON": {"Type": "DOCUMENT"}},
        OutputSerialization={"JSON": {"RecordDelimiter": ","}},
    )

    for event in resp["Payload"]:
        if "Records" in event:
            json_result = event["Records"]["Payload"].decode("utf-8")
        elif "Stats" in event:
            print(event["Stats"])

    # Remove the last trailing comma in the json file to make it a valid json
    if json_result[-1] == ",":
        json_result = json_result[:-1]

    # put the file in the s3 bucket
    s3_client.put_object(Body=json_result, Bucket="ks-test-corp", Key="result.json")
