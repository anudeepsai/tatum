module "ecs_scheduled_task" {
  source  = "git::https://github.com/tmknom/terraform-aws-ecs-scheduled-task?ref=2.0.0"
  name                = "tatum-cron"
  schedule_expression = "rate(3 minutes)"
  cluster_arn         = aws_ecs_cluster.app.id
  subnets             = [aws_subnet.private_d.id, aws_subnet.private_e.id]

  container_definitions = jsonencode([
    {
      name: "tatum-api-cron-task",
      image: "${aws_ecr_repository.tatum_api.repository_url}:latest",
      essential = true
      portMappings = [{
        containerPort: 3000
      }]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = "/ecs/tatum-api"
          awslogs-region        = "us-east-1"
          awslogs-stream-prefix = "date"
        }
      }
    }
  ])

  assign_public_ip = false
}