FROM python:3

RUN  mkdir WORK_REPO
RUN  cd  WORK_REPO
WORKDIR  /WORK_REPO
ADD download.py .
ADD requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

CMD ["python", "-u", "download.py"]