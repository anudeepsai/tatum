# tatum-project



## Getting started

![alt text](tatum.drawio.svg)


[static website](https://resources-s3bucketforstaticcontent-17yj9w6lybghp.s3.amazonaws.com/index.html#)

[extracted JSON](tatum)

## issues faced
- Couldn't read S3 bucket public json file due to CORS issue. fixed it using CORS policy in s3 bucket 
## Other considerations
- Might need to increase memory for reading large chunks of data. ( possible use of threads ?)
- Migrate terraform state to TF cloud
## todo
- use a cloudfront for s3 static website with SSL certificate with route 53 custom domain
- use an ALB with ECS fargate taskf for loadbalancing the traffic to containers
- Export Cloudwatch logs to ELK stack for analysis
- Implement opentelemetry for distributed tracing